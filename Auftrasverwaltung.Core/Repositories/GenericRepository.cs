﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auftragsverwaltung.Data.Data;
using Auftrasverwaltung.Core.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Auftrasverwaltung.Core.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private OrderDbContext _dbContext;
        private DbSet<T> _dbSet;

        public GenericRepository()
        {
            _dbContext = new OrderDbContext();
            _dbSet = _dbContext.Set<T>();
        }

        public GenericRepository(OrderDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return  _dbSet.ToList();
        }

        public T? GetById(object id)
        {
            return _dbSet.Find();
        }

        public void Insert(T? obj)
        {
            _dbSet.Add(obj);
        }

        public void Update(T? obj)
        {
            _dbSet.Update(obj);
        }

        public void Delete(T? obj)
        {
            _dbSet.Remove(obj);
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}

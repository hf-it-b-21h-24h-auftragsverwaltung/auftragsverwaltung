﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auftragsverwaltung.Data.Data;
using Auftrasverwaltung.Core.Contracts;
using Auftrasverwaltung.Core.Repositories;

namespace Auftragsverwaltung.Controllers
{
    public class CustomerController
    {

        private IGenericRepository<Customer> _repository = null;

        public CustomerController()
        {
            _repository = new GenericRepository<Customer>();
        }

        public IEnumerable<Customer> Index()
        {
            var customers = _repository.GetAll();
            return customers;
        }
    }
}

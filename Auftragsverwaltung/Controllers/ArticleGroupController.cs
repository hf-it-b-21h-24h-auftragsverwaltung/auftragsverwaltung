﻿using Auftragsverwaltung.Data.Data;
using Auftrasverwaltung.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auftrasverwaltung.Core.Repositories;

namespace Auftragsverwaltung.Controllers
{
    class ArticleGroupController
    {
        private IGenericRepository<ArticleGroup> _repositoryAg = null;
        private IGenericRepository<VArticleGroupCte> _repositoryAgCte = null;

        public ArticleGroupController()
        {
            _repositoryAg = new GenericRepository<ArticleGroup>();
            _repositoryAgCte = new GenericRepository<VArticleGroupCte>();
        }

        public IEnumerable<ArticleGroup> Index()
        {
            return _repositoryAg.GetAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VArticleGroupCte> GetArticleGroupsCte()
        {
            var articleGroups = _repositoryAgCte.GetAll();
            return articleGroups;
        }
    }
}

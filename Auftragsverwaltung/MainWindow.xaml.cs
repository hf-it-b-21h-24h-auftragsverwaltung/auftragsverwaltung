﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Auftragsverwaltung.Views;

namespace Auftragsverwaltung
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private UserControl _dashboard = null;
        private UserControl _customers = null;
        private UserControl _articles = null;
        private UserControl _articleGroups = null;
        private UserControl _orders = null;


        public MainWindow()
        {
            InitializeComponent();
            InitStateStart();
        }

        private void InitStateStart()
        {
            LblTitle.Content = "Dashboard";
            _dashboard = new Dashboard();
            CtnCtrlMain.Content = _dashboard;
            //load view here

        }


        /// <summary>
        /// Changes to selected view/user control based on the control which has been clicked.
        /// </summary>
        /// <param name="sender">Control which has been triggered</param>
        /// <param name="uc">User Control to build</param>
        private void ChangeView(object sender, UserControl uc)
        {
            string title = (sender as Button).Content.ToString();
            LblTitle.Content = title;
            CtnCtrlMain.Content = uc;
        }

        private void CmdNavDashboard_Click(object sender, RoutedEventArgs e)
        {
            if (_dashboard == null)
            {
                _dashboard = new Dashboard();
            }
            ChangeView(sender, _dashboard);
        }

        private void CmdNavCustomers_Click(object sender, RoutedEventArgs e)
        {
            if (_customers == null)
            {
                _customers = new Customers();
            }
            ChangeView(sender, _customers);
        }

        private void CmdNavArticles_Click(object sender, RoutedEventArgs e)
        {
            if (_articles == null)
                _articles = new Articles();
            ChangeView(sender, _articles);
        }

        private void CmdNavArticleGroups_Click(object sender, RoutedEventArgs e)
        {
            if (_articleGroups == null)
                _articleGroups = new ArticleGroups();
            ChangeView(sender, _articleGroups);
        }

        private void CmdNavOrders_Click(object sender, RoutedEventArgs e)
        {
            if (_orders == null)
                _orders = new Orders();
            ChangeView(sender, _orders);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Auftragsverwaltung.Controllers;

namespace Auftragsverwaltung.Views
{
    /// <summary>
    /// Interaction logic for Customers.xaml
    /// </summary>
    public partial class Customers : UserControl
    {
        private CustomerController cc = new CustomerController();
        public Customers()
        {
            InitializeComponent();
            var customerList = cc.Index();
            LvCustomers.ItemsSource = customerList;
        }
    }
}

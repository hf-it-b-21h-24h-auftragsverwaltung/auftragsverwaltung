﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Auftragsverwaltung.Controllers;
using Auftragsverwaltung.Data.Data;

namespace Auftragsverwaltung.Views
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : UserControl
    {
        public Dashboard()
        {
            InitializeComponent();
            var ac = new ArticleGroupController();
            var articleGroups = ac.GetArticleGroupsCte();

            BindTree(articleGroups, null);
        }


        private void BindTree(IEnumerable<VArticleGroupCte> list, TreeViewItem parentItem)
        {


            //var nodes = list.Where(x => parentNode == null ? x.ParentId == 0 : x.ParentId == int.Parse(parentNode.Value));
            var items = list.Where(x => parentItem == null ? x.TopArticleGroupID == null : x.TopArticleGroupID == int.Parse(parentItem.Tag.ToString()));
            foreach (var item in items)
            {
                var treeItem = new TreeViewItem();
                treeItem.IsExpanded = true;
                if (item.TopArticleGroupID == null)
                {
                    treeItem.Header = item.Name;
                    treeItem.Tag = item.Id;
                    TvArticleGroups.Items.Add(treeItem);
                }
                else
                {
                    treeItem.Header = item.Name;
                    treeItem.Tag = item.Id;
                    parentItem.Items.Add(treeItem);
                }

                BindTree(list, treeItem);
            }
        }
    }
}

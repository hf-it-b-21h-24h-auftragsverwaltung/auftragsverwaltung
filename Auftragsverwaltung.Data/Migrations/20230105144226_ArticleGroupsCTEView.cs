﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Auftragsverwaltung.Data.Migrations
{
    /// <inheritdoc />
    public partial class ArticleGroupsCTEView : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                    CREATE VIEW OrderDb.V_ArticleGroupsCte AS

                    WITH cte_artCat AS (

                        SELECT 

                            a.Id,
                            a.Name,
                            a.TopArticleGroupID

                        FROM
                            OrderDb.ArticleGroups a
                            WHERE TopArticleGroupID IS NULL

                        UNION ALL

                        SELECT 

                            b.Id,
                            b.Name,
                            b.TopArticleGroupID

                        FROM
                            OrderDb.ArticleGroups b
	                            INNER JOIN cte_artCat c
		                            ON c.id = b.TopArticleGroupID
                    )

                    SELECT * FROM cte_artCat;
             ");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP VIEW V_ArticleGroupsCte;");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Auftragsverwaltung.Data.Migrations
{
    /// <inheritdoc />
    public partial class ArticleGroupsTopGroupAdded : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "TopArticleGroupID",
                schema: "OrderDb",
                table: "ArticleGroups",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TopArticleGroupID",
                schema: "OrderDb",
                table: "ArticleGroups");
        }
    }
}

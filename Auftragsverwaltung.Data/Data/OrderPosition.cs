﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auftragsverwaltung.Data.Data
{
    public class OrderPosition
    {
        public uint OrderId { get; set; }
        public uint ArticleId { get; set; }
        public short Position { get; set; }
        public uint Count { get; set; }
        public virtual Order Order { get; set; }
        public virtual Article Article { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auftragsverwaltung.Data.Data
{
    public class VArticleGroupCte
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public uint? TopArticleGroupID { get; set; }
    }
}

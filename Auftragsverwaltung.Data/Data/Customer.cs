﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Auftragsverwaltung.Data.Data
{
    public class Customer
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Password { get; set; }
        public string Street { get; set; }
        public uint ZIP { get; set; }
        public string City { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}

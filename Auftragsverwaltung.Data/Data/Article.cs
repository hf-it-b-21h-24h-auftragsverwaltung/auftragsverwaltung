﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auftragsverwaltung.Data.Data
{
    public class Article
    {
        public uint Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public uint ArticleGroupId { get; set; }

        public virtual ArticleGroup ArticleGroup { get; set; }

        public virtual  ICollection<OrderPosition> OrderPositions { get; set; }

    }
}

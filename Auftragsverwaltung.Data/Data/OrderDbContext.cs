﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Auftragsverwaltung.Data.Data
{
    public class OrderDbContext : DbContext
    {

        private string _connectionString;
        public OrderDbContext() : base()
        {

        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleGroup> ArticleGroups { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderPosition> OrdersPositions { get; set; }
        public DbSet<VArticleGroupCte> VArticleGroupsCte { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=HELGADEV404\\MSSQLZBW01;Database=OrderDb;Trusted_Connection=True;Encrypt=No");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("OrderDb");


            //Articles
            modelBuilder.Entity<Article>().ToTable("Articles");

            //Article Groups
            modelBuilder.Entity<ArticleGroup>().ToTable("ArticleGroups");


            //Customers
            modelBuilder.Entity<Customer>().ToTable("Customers", c => c.IsTemporal());

            //Orders
            modelBuilder.Entity<Order>().ToTable("Orders");

            //Order Positions
            modelBuilder.Entity<OrderPosition>().ToTable("OrderPositions")
                .HasKey(op => new { op.OrderId, op.ArticleId });


            //Views
            modelBuilder.Entity<VArticleGroupCte>().ToTable("V_ArticleGroupsCte", t => t.ExcludeFromMigrations())
                .HasKey(agc => agc.Id);

        }
    }
}

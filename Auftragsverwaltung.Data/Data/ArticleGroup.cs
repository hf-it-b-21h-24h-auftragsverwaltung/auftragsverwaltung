﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auftragsverwaltung.Data.Data
{
    public class ArticleGroup
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public uint? TopArticleGroupID { get; set; }

        public virtual ArticleGroup TopArticleGroup { get; set; }
        public virtual ICollection<ArticleGroup> TopArticleGroups { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
    }
}

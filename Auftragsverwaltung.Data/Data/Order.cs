﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auftragsverwaltung.Data.Data
{
    public class Order
    {
        public uint Id { get; set; }
        public DateTime OrderDate { get; set; }
        public uint CustomerId { get; set; }

        public virtual Customer Customer { get; set; }  
        public virtual ICollection<OrderPosition> OrderPositions { get; set; }

    }
}
